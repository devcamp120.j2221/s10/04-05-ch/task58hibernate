package com.devcamp.task58hibernate.repository;

import com.devcamp.task58hibernate.model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
