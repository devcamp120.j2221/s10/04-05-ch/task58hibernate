package com.devcamp.task58hibernate.controller;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.task58hibernate.model.CVoucher;
import com.devcamp.task58hibernate.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CVoucherController {
    
    @Autowired
    IVoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers() {
        try {
            List<CVoucher> listVouchers = new ArrayList<>();
            voucherRepository.findAll().forEach(listVouchers::add);

            return new ResponseEntity<>(listVouchers, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/vouchers1")
    public List<CVoucher> getVouchers1() {
        List<CVoucher> listVouchers = new ArrayList<>();
        voucherRepository.findAll().forEach(listVouchers::add);

        return listVouchers;
    }
}
