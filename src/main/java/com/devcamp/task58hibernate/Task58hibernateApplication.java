package com.devcamp.task58hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task58hibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task58hibernateApplication.class, args);
	}

}
